<html>
<head>
<title>Lucchett-ino Homepage</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js" integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i" crossorigin="anonymous"></script>
<script src="js/home.js"></script>

<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<style>
</style>
<script>
console.log(sessionStorage);
if(sessionStorage['login'] != "true")
    window.location.href="/";
</script>

</head>

<body>
<nav class="navbar navbar-expand-sm bg-light navbar-light">
  <a class="nav-link" href="#">Home</a>
  <a class="nav-link" href="logs.php">Logs</a>
</nav>

<?php
$data = file_get_contents("http://90.147.188.79/user");
$users = json_decode($data, true)["data"];
?>

<div id="loading" class="loading">Loading&#8230;</div>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">RFID UID</th>
      <th scope="col">NAME</th>
      <th scope="col">SURNAME</th>
	  <th scope="col">PERMISSION</th>
	  <th scope="col">ACTIONS</th>
    </tr>
  </thead>
  <tbody>
	<?php
		for($i=0; $i < count($users); $i++){
			echo '<tr>';
            $_id = $users[$i]["_id"];
			echo ' <td scope="col">' .  $users[$i]["_id"] . '</td>';
			echo ' <td scope="col">' . $users[$i]["uid"] . '</td>';
			echo ' <td scope="col">' . $users[$i]["name"] . '</td>';
			echo ' <td scope="col">' . $users[$i]["surname"] . '</td>';
			$expr = ($users[$i]["valid"])? 'valid' : 'invalid';
			echo ' <td scope="col" style="text-align:center">  <img id="img_'.$_id.'" width="30" height="30" alt="'.$expr.'" src="images/icons/'.$expr.'.png"/></td>';
			echo '<td scope="col">';
            ?>
				<button  onclick="revert('<?php echo $_id ?>')" class="btn btn-lnk"> <img width="30" height="30" src="images/icons/revert.png" alt="revert"/></button>
				<button  onclick="remove('<?php echo $_id ?>')" class="btn btn-lnk"> <img width="30" height="30" src="images/icons/delete.png" alt="delete"/></button>

            </td>
			</tr>
            <?php
		}
	?>
    <tr>
        <form id="form" action='' method="POST" onSubmit="return create_user()">
            <td scope="col"> New User: </td>
            <td> <input id="uid" required type="text" class="form-control" placeholder="RFID UID"> </td>
            <td> <input id="name" required type="text" class="form-control" placeholder="Name"> </td>
            <td> <input id="surname" required type="text" class="form-control" placeholder="Surname"> </td>
            <td>
                <select id="state" required id="inputState" class="form-control">
                    <option value="True">Valid</option>
                    <option selected value="">Invalid</option>
                </select>
            </td>
            <td>
                <button id="submit" type="submit"  class="btn btn-lnk"> <img width="30" height="30" src="images/icons/save.png" alt="create new user"/></button>
                <button  type="reset" class="btn btn-lnk"> <img width="30" height="30" src="images/icons/eraser.png" alt="clear form user"/></button>
            </td>

        </form>
    </tr>
  </tbody>
</table>
</body>

</html>
