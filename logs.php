<html>
<head>
<title>Lucchett-ino Logs</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="js/logs.js"></script>
<script>
console.log(sessionStorage);
if(sessionStorage['login'] != "true")
    window.location.href="/";
</script>


<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<body>
<body>
<nav class="navbar navbar-expand-sm bg-light navbar-light">
  <a class="nav-link" href="home.php">Home</a>
  <a class="nav-link" href="logs.php">Logs</a>
</nav>

<?php

#MACRO
$EL_PER_PAGE = 10;
$TOT_EL = json_decode(file_get_contents("http://90.147.188.79/logs/count"), true)["data"];
$PAGES = ceil($TOT_EL / $EL_PER_PAGE);

$page = $_GET["page"] ?? '1';
$skip = ($page - 1) * $EL_PER_PAGE;

$url = "http://90.147.188.79/logs?skip=".$skip."&limit=".$EL_PER_PAGE;
$data = file_get_contents($url);
$logs = json_decode($data, true)["data"];
?>

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">NAME</th>
      <th scope="col">SURNAME</th>
      <th scope="col">RFID ID</th>
      <th scope="col">STATUS</th>
	  <th scope="col">TIMESTAMP</th>
    </tr>
  </thead>
  <tbody>
	<?php
        for($i=0; $i < count($logs); $i++)
        {
			echo '<tr>';
			echo ' <td scope="col">' .  $logs[$i]["name"] . '</td>';
			echo ' <td scope="col">' . $logs[$i]["surname"] . '</td>';
            echo ' <td scope="col">' . $logs[$i]["uid"] . '</td>';
            
            $expr = ($logs[$i]["status"]== "200")? 'opendoor' : 'closedoor';
            echo ' <td scope="col">' . $logs[$i]["status"] . '<img width="30" height="30" src="images/icons/'.$expr.'.png"/> </td>';
            echo ' <td scope="col">' . $logs[$i]["timestamp"] . '</td>';
        }
		
    ?>
   </tbody>







<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-end">
    <li class="page-item">
      <a class="page-link" href="logs.php">Start</a>
    </li>

    <?php
    $firstpage = $page -2;
    if ($firstpage < 1)
        $firstpage = 1;

    for($i=$firstpage; $i<=$PAGES && $i< $firstpage+5; $i++)
        echo '<li class="page-item"><a class="page-link" href="logs.php?page='.$i.'">'.$i.'</a></li>'
    ?>
    <li class="page-item">
      <a class="page-link" href="logs.php?page=<?php echo $PAGES?>">End</a>
    </li>
  </ul>
</nav> 
<!-- bootstrap js -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
