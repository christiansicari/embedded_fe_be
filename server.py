
from pymongo import MongoClient, DESCENDING
from bottle import route, get, post, abort, response, request, run, app, HTTPResponse
from bson.objectid import ObjectId
from datetime import datetime
import paste
port ="80"
#db_data = {"host":"locahost:8081", "user":"root", "pw":"HelloMario", "db":"embedded"}

client = MongoClient()
db = client["embedded"]

def enable_cors(fn):
    def _enable_cors(*args, **kwargs):
        # set CORS headers
        response.headers['Cache-Control'] = 'no-cache'
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Methods'] = 'GET, DELETE, POST, PUT, OPTIONS'
        response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token, key'
        if request.method != 'OPTIONS':
            # actual request; reply with the actual response
            return fn(*args, **kwargs)

    return _enable_cors

def log(req, uid, status):
    now = datetime.now().strftime("%Y-%m-%dT%H:%M")
    
    if req:
        doc = req.copy()
        del doc["valid"]
    else:
        doc = {"name":"Unknown", "surname": "Unknown"}
    doc["timestamp"] = now
    doc["uid"] = uid
    doc["status"] = status

    db.logs.insert_one(doc)


def _check_uid(uid):
    print("uid: ", uid)
    query = {"uid":uid}
    res = db.users.find_one(query)
    print("res: ", res)
    try:
        return {"name": res["name"], "surname": res["surname"], "valid":res["valid"]}
    except:
       return None

@post("/unity")
def posting():
    data = request.json
    print(data)

@get("/user")
def get_user():
    res = db.users.find({})
    docs = []
    for doc in res:
        doc["_id"] = str(doc["_id"])
        docs.append(doc)
    return {"data": docs }

@route("/user", method=["POST","OPTIONS"])
@enable_cors
def post_user():
    user = request.json
    print("user: ", user)
    try:
        user["valid"] = bool(user["valid"])
        print("user2: ", user)
        response = db.users.insert_one(user)
        print("response: ", response)

    except Exception as e:
        print("exception: ", e)
        return HTTPResponse(status=400)

@get("/revert/<_id>")
@enable_cors
def revert(_id):
    print("revert id: ",_id)
    try:
        _id = ObjectId(_id)
        value = db.users.find_one({"_id":_id})["valid"]
        res = db.users.update_one({"_id":_id}, {"$set":{"valid": not value}})
        print("res: ", res)
        return {"response":True}
    except Exception as error:
        print("error: ", error)
        return {"response":False}

@route("/user/<_id>", method=["DELETE","OPTIONS"])
@enable_cors
def remove(_id):
    _id = ObjectId(_id)
    try:
        db.users.remove({"_id":_id})
    except:
        return HTTPResponse(status=400)

@route("/logs/count")
@enable_cors
def get_logs_count():
    count = db.logs.count({})
    return {"data": count}

@route("/logs")
@enable_cors
def get_logs():

    try:
        skip = int(request.params["skip"])
        limit = int(request.params["limit"])
    except:
        return HTTPResponse(status=500, body="parameters must be integer numbers")

    res = db.logs.find({}, {"_id":0}).sort([("timestamp", DESCENDING)]).skip(skip).limit(limit)
    data = [doc for doc in res]
    return {"data": data}

@get("/")
def root():
    return "Hello, I am online"
@get("<uid/<uid>/")
@get("/uid/<uid>")
def uid(uid):
    h = [ (k, request.headers[k]) for k in request.headers]
    print("headers: ",  h)
    res = _check_uid(uid)
    if res:
        name = res["name"]
        surname = res["surname"]
        body = name[0] + ".  " + surname
        status = ( 200 if res["valid"] else 403)
       
    else:
        status = 404
        body = "RFID Not Found"
    print("status: ", status)
    log(res, uid, status)
    return str(status) + "," + body
    return HTTPResponse(status=status, body=body)


if __name__ == "__main__":
    print("Running Server...")
    run(host="0.0.0.0", port=port,server="paste")
    #run(host="0.0.0.0", port=port)
